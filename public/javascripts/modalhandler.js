function showMyMeny(){
    if(!$('#profilmeny').is('visible')){
        $(document).one("click","body", function(){
            $('#profilmeny').hide('fast');
        });
    }
    $('#profilmeny').toggle('fast');
}

function showAdminMeny(){
    if(!$('#administrasjonsmeny').is('visible')){
        $(document).one("click","body", function(){
            $('#administrasjonsmeny').hide('fast');
        });
    }
    $('#administrasjonsmeny').toggle('fast');
}

function closeModal(){
    $('#modal').hide('fast');
    $('#dark-overlay').hide('fast');
    $('#modal .result').html('Modal skal være lukket...');
    $('#modal .result').hide();
    $('#modal .loader').show();
}

function showModal(){
    $('#modal').show('fast');
    $('#dark-overlay').show();
    $('#modal .loader').show('fast');
}

function visAdresseinformasjon(){
    showModal();

    $('#modal .result').load('/ModalController/adresseinformasjon', function(){
        $('#modal .result').show('fast');
        $('#modal .loader').hide();
    });
}

function visAvtalestatus(){
    showModal();

    $('#modal .result').load('/ModalController/avtalestatus', function(){
        $('#modal .result').show('fast');
        $('#modal .loader').hide();
    });
}

function visLeggTilBruker(){
    showModal();

    $('#modal .result').load('/ModalController/leggTilBruker', function(){
        $('#modal .result').show('fast');
        $('#modal .loader').hide();
    });
}

function visBrukeroversikt(){
    showModal();

    $('#modal .result').load('/AdminController/visAlleBrukere', function(){
        $('#modal .result').show('fast');
        $('#modal .loader').hide();
    });
}

