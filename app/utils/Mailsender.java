package utils;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import play.libs.Mail;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 03.08.13
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */
public class Mailsender {

    public static boolean sendGlemtPassord(String epost, String passord) throws EmailException {
        SimpleEmail email = new SimpleEmail();
        email.setFrom("admin@bruland.it");
        email.addTo("epost");
        email.setSubject("Glemt passord");
        email.setMsg("Ditt passord er: " + passord);
        Mail.send(email);
        return true;
    }
}
