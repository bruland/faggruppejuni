package Bootstrap;

import models.Bruker;
import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 02.05.13
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */

@OnApplicationStart
public class UserSetup extends Job {
     public void doJob(){
         if(Bruker.count() == 0){
            Logger.debug("Forsøker å laste in default-user.yml");
             Fixtures.loadModels("default-users.yml");

         }
     }
}
