package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import java.io.File;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 02.08.13
 * Time: 10:06
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class Vedlegg extends Model {
    public Date opprettet;
    public String navn;

    @Lob
    public byte[] file;

    public Vedlegg(byte[] file, String navn){
        this.file = file;
        this.opprettet = new Date();
        this.navn = navn;
    }
}
