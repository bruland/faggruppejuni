package models;


import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 02.05.13
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class Bruker extends Model {
    public String username;
    public String password;
    public String navn;
    public String telefonnummer;
    public String epost;
    public String adresse; //med postnummer
    public boolean isAdmin; //default false
    public boolean deaktivert;
    public Date sisteInnloggingsdato;


    @Column( length = 2140000000 )
    public ArrayList<Vedlegg> vedleggsliste;


    public Bruker(String username, String password) {
        this.username = username;
        this.password = password;
        this.vedleggsliste = new ArrayList<Vedlegg>();
    }

    public Bruker() {
        this.vedleggsliste = new ArrayList<Vedlegg>();
    }

    public void leggTilVedlegg(Vedlegg file) {
        if(vedleggsliste == null){
            vedleggsliste = new ArrayList<Vedlegg>();
        }
        vedleggsliste.add(file);
        save();

    }

    @Override
    public String toString() {
        return "Bruker{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", navn='" + navn + '\'' +
                ", telefonnummer='" + telefonnummer + '\'' +
                ", epost='" + epost + '\'' +
                ", adresse='" + adresse + '\'' +
                ", vedleggsliste=" + vedleggsliste +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
