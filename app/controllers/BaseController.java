package controllers;

import org.junit.After;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 02.08.13
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */
public class BaseController extends Controller {

    //todo lag en before metode her som logger ip-adresse til besøkende, nettleser ++????
    @Before             //hvorfor fungerer ikke after?
    static void logVisitorDetails(){
        Logger.debug("#####################      Visitor details start      #######################");
        Logger.debug("Tidspunkt: " + new Date());
        Logger.debug("Visitor ip: " + request.remoteAddress);

        for(String key: request.headers.keySet()){
            Logger.debug(key +": " + request.headers.get(key));
        }

        Logger.debug("#####################      Visitor details ended      #######################");
    }


}
