package controllers;

import models.Bruker;
import play.mvc.Controller;
import play.mvc.With;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 02.08.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */

@With(SecurityController.class)
public class ModalController extends Controller {
    public static void adresseinformasjon(){
        Bruker user = SecurityController.getCurrentUser();
        renderTemplate("modals/adresseinformasjon.html", user);
    }

    public static void avtalestatus(){
        Bruker user = SecurityController.getCurrentUser();
        renderTemplate("modals/avtalestatus.html", user);
    }

    public static void oppdaterAdresseInfo(String navn, String telefonnummer, String epost, String adresse){
        Bruker user = SecurityController.getCurrentUser();
        user.navn = navn;
        user.telefonnummer = telefonnummer;
        user.epost = epost;
        user.adresse = adresse;
        user.save();
    }

    public static void leggTilBruker(){
        Bruker user = SecurityController.getCurrentUser();
        if(user.isAdmin){
            renderTemplate("modals/leggTilBruker.html");
        }else{
            badRequest();
        }
    }


}
