package controllers;

import play.i18n.Lang;
import play.mvc.Controller;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 13.09.13
 * Time: 19:08
 * To change this template use File | Settings | File Templates.
 */
public class LanguageController extends Controller {
    public static void setNorwegian(){
        Lang.change("no");
    }

    public static void setEnglish(){
        Lang.change("en");
    }

}
