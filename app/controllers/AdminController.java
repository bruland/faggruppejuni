package controllers;

import models.Bruker;
import play.data.validation.Email;
import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.With;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kjetilbruland
 * Date: 03.08.13
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */

@With(SecurityController.class)
public class AdminController extends Controller {

    public static void leggTilBruker(@Required String kontaktperson, @Required @Email String epost,
                                     @Required String telefonnummer, @Required String adresse, @Required String passord){
        if(validation.hasErrors()){
            badRequest();
        }else{
            Bruker bruker = new Bruker();
            bruker.username = epost;
            bruker.epost = epost;
            bruker.navn = kontaktperson;
            bruker.telefonnummer = telefonnummer;
            bruker.adresse = adresse;
            bruker.password = passord;
            bruker.save();
            renderTemplate("AdminController/nyBrukerOpprettet.html", bruker);
        }
    }

    public static void visAlleBrukere(){
        List<Bruker> brukere = Bruker.findAll();
        renderTemplate("AdminController/visAlleBrukere.html",brukere);

    }
}
