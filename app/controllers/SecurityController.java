package controllers;

import org.apache.commons.mail.EmailException;
import play.Logger;
import play.mvc.*;

import models.*;
import utils.Mailsender;

import java.util.Date;


@With(BaseController.class)
public class SecurityController extends Controller {

    @Before(unless = {"index", "login", "glemtPassord"})
    static void sjekkAtBrukerErInnlogget(){
        if(!session.contains("user")){
            Logger.debug("Bruker er ikke logget inn, sender bruker til SecurityController.index()");
            SecurityController.index();
        }
    }

    public static void login(String username, String password){
        Bruker current = Bruker.find("byUsername", username).first();
        if(current != null && current.password.equals(password)){
            session.put("user", current.username);
            current.sisteInnloggingsdato = new Date();
            current.save();
            PageOneController.hoved();
        }else{
            renderTemplate("SecurityController/badboy.html");
        }
    }

    public static void logout(){
        session.clear();
        index();
    }

    public static void index() {
        render();
    }

    public static Bruker getCurrentUser(){
        return Bruker.find("byUsername", session.get("user")).first();
    }

    public static void glemtPassord(String epost){
        Bruker bruker = Bruker.find("byUsername", epost).first();
        if(bruker != null && bruker.epost.equals(epost)){
            try {
                Mailsender.sendGlemtPassord(epost, bruker.password);
            } catch (EmailException e) {
                Logger.error("Det oppstod en feil ved sending av epost. Bruker er varslet om generell feil i systemet.");
                error(500, "Det oppstod en feil ved sending av epost. Vi har loggført feilen.");
            }

        }
        render();
    }

}